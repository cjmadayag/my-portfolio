const textIndicator = document.querySelector(".textIndicator");
let isIndicatorShowing = true;

setInterval(() => {
    isIndicatorShowing = !isIndicatorShowing;
    if(isIndicatorShowing){
        textIndicator.classList.add("hideTextIndicator");
    }else{
        textIndicator.classList.remove("hideTextIndicator");
    }
}, 500);

const msg = ["Hi","My name is Celmer","I am a Web Developer"]
const textLP = document.querySelector(".textLP");

if(msg.length>0){
    printText(msg[0])
}

let concat =""

function printText(text){
    setTimeout(()=>{
        if(text.length>0){
            concat += text[0]
            isIndicatorShowing = true;
            textLP.textContent = concat
            text = text.substr(1);
            printText(text)
        }else{
            concat = "";
            const {length} = msg[0].replace(/ /g,"");

            msg.push(msg[0]);
            msg.shift();
            if(msg.length>0){
                isIndicatorShowing = false;
                setTimeout(()=>{
                    printText(msg[0]);
                },100*length)
            }
        }
    },100)
}

function runAnimation(isMobile){
	if(isMobile){
		document.getElementById("landingPage").classList.add("animationLandingPage");
	}else{
		document.getElementById("left").classList.add("animationLeft");
		document.getElementById("right").classList.add("animationRight");
	}
	
	document.getElementById("navDesktop").classList.add("lg:flex","animateNav");
	document.getElementById("navMobile").classList.remove("hidden");
	document.getElementById("navMobile").classList.add("animateMobileNav");
	
	setTimeout(()=>{
		document.getElementById("content").style.zIndex=0;

		let homeLi = document.getElementById("homeLi");

		homeLi.classList.add("animateHome");
		homeLi.nextElementSibling.classList.add("animateHr");
		homeLi.nextElementSibling.nextElementSibling.classList.add("animateProjects");

		document.getElementById("home").firstElementChild.classList.add("animateTop");
		getLogos();
	}, 1600);
}

function getLogos(){
	fetch("assets/lib/skills.json").then(res=>res.json()).then(res=>{
		logo(0,res);

		function logo(index,array){
			setTimeout(function(){
				let img = document.createElement("img");
				img.src = array[index];
				img.setAttribute("class","animateLogo m-auto");
				document.getElementById("botContent").lastElementChild.firstElementChild.appendChild(img);
				index++;
				if(index!=array.length){
					logo(index,array);
				};
			},300);			
		}
	});
}

function home(){
	let project = document.getElementById("project");
	let home =  document.getElementById("home");
	let homeLi = document.getElementById("homeLi").firstElementChild;
	let projectLi = document.getElementById("projectLi").firstElementChild;
	let homeLiMobile = document.getElementById("homeLiMobile").firstElementChild;
	let projectLiMobile = document.getElementById("projectLiMobile").firstElementChild;

	if(project.classList.contains("moveProject")){
		project.classList.remove("moveProject");
		project.classList.add("hideProject");
		home.classList.remove("hideHome");
		home.classList.add("showHome");
		projectLi.classList.remove("indicator");
		homeLi.classList.add("indicator");
		projectLiMobile.classList.remove("indicator");
		homeLiMobile.classList.add("indicator");
	}
}

function project(){
	let project = document.getElementById("project");
	let home =  document.getElementById("home");
	let homeLi = document.getElementById("homeLi").firstElementChild;
	let projectLi = document.getElementById("projectLi").firstElementChild;
	let homeLiMobile = document.getElementById("homeLiMobile").firstElementChild;
	let projectLiMobile = document.getElementById("projectLiMobile").firstElementChild;

	if(project.classList.contains("hideProject")){
		project.classList.remove("hideProject","hidden");
		project.classList.add("moveProject");
		home.classList.add("hideHome");
		home.classList.remove("showHome");
		homeLi.classList.remove("indicator");
		projectLi.classList.add("indicator");
		homeLiMobile.classList.remove("indicator");
		projectLiMobile.classList.add("indicator");
	}
}

const projArray = document.querySelectorAll(".proj");
projArray.forEach(proj=>{
	proj.firstElementChild.addEventListener("click",()=>{
		window.open(proj.dataset.url, '_blank');
	})
	proj.lastElementChild.addEventListener("click",()=>{
		window.open(proj.dataset.url, '_blank');
	})
})